package com.gsafety.devops.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Validator;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Workbook;
import org.jeecgframework.core.common.controller.BaseController;
import org.jeecgframework.core.common.exception.BusinessException;
import org.jeecgframework.core.common.hibernate.qbc.CriteriaQuery;
import org.jeecgframework.core.common.model.json.AjaxJson;
import org.jeecgframework.core.common.model.json.DataGrid;
import org.jeecgframework.core.constant.Globals;
import org.jeecgframework.core.util.DynamicDBUtil;
import org.jeecgframework.core.util.ExceptionUtil;
import org.jeecgframework.core.util.MyBeanUtils;
import org.jeecgframework.core.util.ResourceUtil;
import org.jeecgframework.core.util.StringUtil;
import org.jeecgframework.core.util.oConvertUtils;
import org.jeecgframework.poi.excel.ExcelExportUtil;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.entity.vo.NormalExcelConstants;
import org.jeecgframework.tag.core.easyui.TagUtil;
import org.jeecgframework.web.system.pojo.base.DynamicDataSourceEntity;
import org.jeecgframework.web.system.service.SystemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.gsafety.devops.entity.SqlListEntity;
import com.gsafety.devops.entity.SqlTaskEntity;
import com.gsafety.devops.page.SqlListPage;
import com.gsafety.devops.service.SqlListServiceI;

import io.swagger.annotations.Api;

/**   
 * @Title: controller
 * @Description: 执行任务
 * @author onlineGenerator
 * @date 2018-09-26 16:12:20
 * @version V1.0   
 *
 */

@Api(value="SqlTask",description="执行记录",tags="sqlTaskController")
@Controller
@RequestMapping("/sqlTaskController")
public class SqlTaskController extends BaseController {
	
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(SqlTaskController.class);

	@Autowired
	private SqlListServiceI sqlListService;
	@Autowired
	private SystemService systemService;
	@Autowired
	private Validator validator;

	/**
	 * 执行任务列表 页面跳转
	 * @return
	 */
	@RequestMapping(params = "list")
	public ModelAndView list(HttpServletRequest request) {
		return new ModelAndView("com/gsafety/devops/sqlTask-list");
	}
	
	/**
	 * 跳转可排序多选数据库选择界面
	 * @return
	 */
	@RequestMapping(params = "dbGridSelect")
	public ModelAndView dbGridSelect() {
		return new ModelAndView("com/gsafety/devops/sqlTask-dbGridSelect");
	}
	
	/**
	 * 执行SQL
	 * @return
	 */
	@RequestMapping(params = "executeSql")
	@ResponseBody
	public AjaxJson executeSql(String sqlId, String dbIds, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		String message = "执行SQL";
		try{
			SqlListEntity sql = systemService.get(SqlListEntity.class, sqlId);
			
			String[] dbIdArr = dbIds.split(",");
			for (String dbId : dbIdArr) {
				DynamicDataSourceEntity db = systemService.get(DynamicDataSourceEntity.class, dbId);
				String execResult = "";
				try {
					DynamicDBUtil.batchUpdate(db.getDbKey(), sql.getSqlContent().split(";"));
					execResult = "Success";
				} catch(NullPointerException e){
					execResult = "获取数据源失败";
				} catch (Exception e) {
					e.printStackTrace();
					execResult = e.getMessage();
				}
				
				SqlTaskEntity sqlTask = new SqlTaskEntity();
				sqlTask.setSqlId(sqlId);
				sqlTask.setDbId(dbId);
				sqlTask.setExecuteFlag(1);
				sqlTask.setExecuteResult(execResult);
				sqlTask.setExecuteDate(new Date());
				systemService.save(sqlTask);
			}
			
			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			
		}catch(Exception e){
			e.printStackTrace();
			message = "执行SQL失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 可排序多选界面查数据库表放在Datagrid中
	 * @param user
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(params = "dbListGrid", method = RequestMethod.POST)
	@ResponseBody
	public void getDbListGrid(DynamicDataSourceEntity db,HttpServletRequest request,HttpServletResponse response,DataGrid dataGrid)throws Exception{
		CriteriaQuery cq = new CriteriaQuery(DynamicDataSourceEntity.class, dataGrid);
        //查询条件组装器
        org.jeecgframework.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, db);
        this.systemService.getDataGridReturn(cq, true);
        TagUtil.datagrid(response, dataGrid);
	}
	

	/**
	 * easyui AJAX请求数据
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */
	@RequestMapping(params = "datagrid")
	public void datagrid(SqlTaskEntity sqlTask,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(SqlTaskEntity.class, dataGrid);
		String mainId = request.getParameter("mainId");
		if(oConvertUtils.isNotEmpty(mainId)){
			//查询条件组装器
			org.jeecgframework.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, sqlTask,request.getParameterMap());
			try{
				//自定义追加查询条件
			 	cq.eq("sqlId", mainId);
			}catch (Exception e) {
				throw new BusinessException(e.getMessage());
			}
			cq.add();
			this.sqlListService.getDataGridReturn(cq, true);
		}
		TagUtil.datagrid(response, dataGrid);
	}
	
	/**
	 * 删除执行任务
	 * @return
	 */
	@RequestMapping(params = "doDel")
	@ResponseBody
	public AjaxJson doDel(SqlTaskEntity sqlTask, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		sqlTask = systemService.getEntity(SqlTaskEntity.class, sqlTask.getId());
		String message = "执行任务删除成功";
		try{
			if(sqlTask!=null){
				sqlListService.delete(sqlTask);
				systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
			}
		}catch(Exception e){
			e.printStackTrace();
			message = "执行任务删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 批量删除执行任务
	 * @return
	 */
	@RequestMapping(params = "doBatchDel")
	@ResponseBody
	public AjaxJson doBatchDel(String ids,HttpServletRequest request){
		AjaxJson j = new AjaxJson();
		String message = "执行任务删除成功";
		try{
			for(String id:ids.split(",")){
				SqlTaskEntity sqlTask = systemService.getEntity(SqlTaskEntity.class,
				id
				);
				if(sqlTask!=null){
					sqlListService.delete(sqlTask);
					systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
				}
				
			}
		}catch(Exception e){
			e.printStackTrace();
			message = "执行任务删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 添加执行任务
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doAdd")
	@ResponseBody
	public AjaxJson doAdd(String dbIds, SqlTaskEntity sqlTask, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		String message = "添加成功";
		try{
			String[] dbIdArr = dbIds.split(",");
			List<SqlTaskEntity> sqlTaskList = new ArrayList<SqlTaskEntity>();
			for (String dbId : dbIdArr) {
				SqlTaskEntity t = new SqlTaskEntity();
				t.setSqlId(sqlTask.getSqlId());
				t.setDbId(dbId);
				t.setExecuteFlag(sqlTask.getExecuteFlag());
				sqlTaskList.add(t);
			}
			
			sqlListService.batchSave(sqlTaskList);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "执行任务添加失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 更新执行任务
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doUpdate")
	@ResponseBody
	public AjaxJson doUpdate(SqlTaskEntity sqlTask, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		String message = "更新成功";
		SqlTaskEntity t = sqlListService.get(SqlTaskEntity.class,sqlTask.getId());
		try{
			MyBeanUtils.copyBeanNotNull2Bean(sqlTask, t);
			sqlListService.saveOrUpdate(t);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "执行任务更新失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	
	/**
	 * 执行任务编辑页面跳转
	 * @return
	 */
	@RequestMapping(params = "goUpdate")
	public ModelAndView goUpdate(SqlTaskEntity sqlTask, HttpServletRequest request){
		if (StringUtil.isNotEmpty(sqlTask.getId())) {
			sqlTask = sqlListService.getEntity(SqlTaskEntity.class, sqlTask.getId());
			request.setAttribute("sqlTaskPage", sqlTask);
		}
		return new ModelAndView("com/gsafety/devops/sqlTask-update");
	}
	
	/**
	 * 行编辑保存操作
	 * @param page
	 * @return
	 */
	@RequestMapping(params = "saveRows")
	@ResponseBody
	public AjaxJson saveRows(SqlListPage page,HttpServletRequest req){
		String message = "操作成功！";
		List<SqlTaskEntity> lists=page.getSqlTaskList();
		AjaxJson j = new AjaxJson();
		String mainId = req.getParameter("mainId");
		if(CollectionUtils.isNotEmpty(lists)){
			for(SqlTaskEntity temp:lists){
				if (StringUtil.isNotEmpty(temp.getId())) {
					SqlTaskEntity t =this.systemService.get(SqlTaskEntity.class, temp.getId());
					try {
						MyBeanUtils.copyBeanNotNull2Bean(temp, t);
						systemService.saveOrUpdate(t);
						systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					try {
						//temp.setDelFlag(0);若有则需要加
						temp.setSqlId(mainId);
						systemService.save(temp);
						systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				}
			}
		}
		return j;
	}
	
	/**
	 * 导出excel
	 * @param request
	 * @param response
	 */
    @RequestMapping(params = "exportXls")
    public void exportXls(SqlTaskEntity sqlTask,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid,ModelMap map) throws Exception {
    	CriteriaQuery cq = new CriteriaQuery(SqlTaskEntity.class, dataGrid);
    	//必须要有合同id
		String mainId = request.getParameter("mainId");
		List<SqlTaskEntity> list =new ArrayList<SqlTaskEntity>();
		if(oConvertUtils.isNotEmpty(mainId)){
			//查询条件组装器
			sqlTask.setSqlId(mainId);
			org.jeecgframework.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, sqlTask, request.getParameterMap());
			try{
				//自定义追加查询条件
				//cq.eq("delFlag", 0);
			}catch (Exception e) {
				e.printStackTrace();
				throw new BusinessException(e.getMessage());
			}
			cq.add();
			list= this.systemService.getListByCriteriaQuery(cq, false);
			Workbook excel=ExcelExportUtil.exportExcel(new ExportParams(), SqlTaskEntity.class, list);
			response.setContentType("application/x-msdownload;charset=utf-8");
			response.setHeader("Content-disposition", "attachment; filename="+new String("执行任务列表.xls".getBytes("UTF-8"), "iso-8859-1"));
			OutputStream outputStream = null;
			try {
				outputStream = response.getOutputStream();
				excel.write(outputStream);
			} catch (IOException e) {
				e.printStackTrace();
			}finally{
				try {
					if(outputStream!=null)outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
   /**
	* 导出excel 使模板
	*/
	@RequestMapping(params = "exportXlsByT")
	public String exportXlsByT(ModelMap map) {
		map.put(NormalExcelConstants.FILE_NAME,"执行任务");
		map.put(NormalExcelConstants.CLASS,SqlTaskEntity.class);
		map.put(NormalExcelConstants.PARAMS,new ExportParams("执行任务列表", "导出人:"+ ResourceUtil.getSessionUser().getRealName(),"导出信息"));
		map.put(NormalExcelConstants.DATA_LIST,new ArrayList());
		return NormalExcelConstants.JEECG_EXCEL_VIEW;
	}

    /**
	 * 通过excel导入数据
	 * @param request
	 * @param
	 * @return
	 */
	@RequestMapping(params = "importExcel", method = RequestMethod.POST)
	@ResponseBody
	public AjaxJson importExcel(HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		String mainId = request.getParameter("mainId");
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
			MultipartFile file = entity.getValue();// 获取上传文件对象
			ImportParams params = new ImportParams();
			params.setTitleRows(2);
			params.setHeadRows(2);
			params.setNeedSave(true);
			try {
				List<SqlTaskEntity> list =  ExcelImportUtil.importExcel(file.getInputStream(), SqlTaskEntity.class, params);
				for (SqlTaskEntity page : list) {
					page.setSqlId(mainId);
		       		sqlListService.save(page);
				}
				j.setMsg("文件导入成功！");
			} catch (Exception e) {
				j.setMsg("文件导入失败！");
				logger.error(ExceptionUtil.getExceptionMessage(e));
			}finally{
				try {
					file.getInputStream().close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return j;
	}
	
}
